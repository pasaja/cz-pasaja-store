Release Name: cz-pasaja-store v2019.0.0

cz-pasaja-store
Description: PASAJA data storage server based on WUI Frameworks's hub
Author: PASAJA Authors
License: BSD-3-Clause. See LICENSE.txt
Format: executable binary and embedded resources
Location: [application-name][.exe],
          robots.txt,
          [application-name][.exe]/package.json,
          [application-name][.exe]/resource/css/[package-name]-[version].min.css,
          [application-name][.exe]/resource/graphics/icon.ico,
          [application-name][.exe]/resource/javascript/[package-name]-[version].d.ts,
          [application-name][.exe]/resource/javascript/[package-name]-[version].min.js,
          [application-name][.exe]/resource/javascript/[package-name]-[version].min.js.map,
          [application-name][.exe]/resource/javascript/loader.min.js,
          [application-name][.exe]/test/resource/data/Cz/Pasaja/Store

nodemailer >= 4.6.7
Description: Easy as cake e-mail sending from your Node.js applications
Author: Andris Reinman
License: MIT. See LICENSE
Format: source code
Location: [application-name][.exe]/node_modules/nodemailer

com-wui-framework-localhost v2019.0.1
Description: WUI Framework's code base for localhost based on Node.js
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: executable binary and embedded resources
Location: [application-name][.exe],
          [application-name][.exe]/package.json,
          [application-name][.exe]/resource/css/[package-name]-[version].min.css,
          [application-name][.exe]/resource/graphics/Com/Wui/Framework/Localhost,
          [application-name][.exe]/resource/graphics/icon.ico,
          [application-name][.exe]/resource/javascript/[package-name]-[version].d.ts,
          [application-name][.exe]/resource/javascript/[package-name]-[version].min.js,
          [application-name][.exe]/resource/javascript/[package-name]-[version].min.js.map,
          [application-name][.exe]/resource/javascript/loader.min.js,
          [application-name][.exe]/resource/javascript/xservices.js,
          [application-name][.exe]/test/resource/data/Com/Wui/Framework/Localhost

com-wui-framework-nodejsre v2019.0.1
Description: Runtime environment for headless mod based on Node.js instance
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: executable binary
Location: node[.dll]
          node[.lib]

Node.js v10.15.0
Description: JavaScript runtime built on Chrome's V8 JavaScript engine
Author: Node.js authors
License: MIT license. See Nodejs_LICENSE.txt
Format: executable binary and source code
Location: node[.exe]
          node[.dll]
          node[.lib]
          include

npm v6.4.1
Description: JavaScript runtime built on Chrome's V8 JavaScript engine
Author: npm, Inc. and Contributors
License: The Artistic License 2.0. See LICENSE
Format: source code
Location: node_modules/npm,
          npm[.cmd]

[OPTIONAL]
libelf v0.8.12
Description: Free ELF object file access library
Author: Michael "Tired" Riepe
License: GNU LGPL. See COPYING.LIB
Format: compiled source code
Location: resource/libs/libelf/libelf.so

decompress >= 4.2.0
Description: Extracting archives made easy
Author: Kevin Mårtensson
License: MIT. See license
Format: source code
Location: [application-name][.exe]/node_modules/decompress

decompress-tarxz >= 2.1.1
Description: tar.xz decompress plugin
Author: Kevin Mårtensson
License: MIT. See license
Format: source code
Location: [application-name][.exe]/node_modules/decompress-tarxz

archiver >= 3.0.0
Description: a streaming interface for archive generation
Author: Chris Talkington
License: MIT. See LICENSE
Format: source code
Location: [application-name][.exe]/node_modules/archiver

compressjs >= 1.0.3
Description: fast pure-JavaScript implementations of various de/compression algorithms
Author: C. Scott Ananian
License: GNU GPL2. See LICENSE.txt
Format: source code
Location: [application-name][.exe]/node_modules/compressjs

colors >= 1.3.0
Description: get colors in your node.js console.
Author: Marak Squires
License: MIT. See LICENSE
Format: source code
Location: [application-name][.exe]/node_modules/colors

source-map-support >= 0.5.6
Description: Fixes stack traces for files with source maps
Author: Evan Wallace
License: MIT. See LICENSE.md
Format: source code
Location: [application-name][.exe]/node_modules/source-map-support

jsdom >= 11.11.0
Description: A JavaScript implementation of many web standards
Author: Elijah Insua
License: MIT. See LICENSE.txt
Format: source code
Location: [application-name][.exe]/node_modules/jsdom

websocket >= 1.0.26
Description: Websocket Client & Server Library implementing the WebSocket protocol as specified in RFC 6455.
Author: Brian McKelvey
License: Apache-2.0. See LICENSE
Format: source code
Location: [application-name][.exe]/node_modules/websocket

node-localstorage >= 1.3.1
Description: A drop-in substitute for the browser native localStorage API that runs on node.js.
Author: Lawrence S. Maccherone, Jr.
License: NA
Format: source code
Location: [application-name][.exe]/node_modules/node-localstorage

strip-json-comments >= 2.0.1
Description: Strip comments from JSON. Lets you use comments in your JSON files!
Author: Sindre Sorhus
License: MIT. See license
Format: source code
Location: [application-name][.exe]/node_modules/strip-json-comments

is-elevated >= 2.0.1
Description: Check if the process is running with elevated privileges
Author: Sindre Sorhus
License: MIT. See license
Format: source code
Location: [application-name][.exe]/node_modules/is-elevated

glob >= 7.1.2
Description: A little globber
Author: Isaac Z. Schlueter and Contributors
License: ISC. See LICENSE
Format: source code
Location: [application-name][.exe]/node_modules/glob

stack-trace >= 0.0.10
Description: Get v8 stack traces as an array of CallSite objects.
Author: Felix Geisendorfer
License: MIT. See License
Format: source code
Location: [application-name][.exe]/node_modules/stack-trace

[OPTIONAL]
win-version-info >= 2.0.0
Description: Windows-only native addon to read version info from executables
Author: Vincent Weevers
License: MIT. See LICENSE
Format: source code
Location: [application-name][.exe]/node_modules/win-version-info
          resource/libs/win-version-info

[OPTIONAL]
windows-build-tools >= 3.0.1
Description: Install C++ Build Tools for Windows using npm
Author: Felix Rieseberg and Microsoft Corporation
License: MIT. See LICENSE
Format: source code
Location: [application-name][.exe]/node_modules/windows-build-tools

com-wui-framework-commons v2019.0.1
Description: Commons library for WUI Framework front-end projects.
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: resource/css/[package-name]-[version].min.css,
          resource/graphics/Com/Wui/Framework/Commons,
          resource/graphics/icon.ico,
          resource/javascript/[package-name]-[version].d.ts,
          resource/javascript/[package-name]-[version].min.js,
          resource/javascript/[package-name]-[version].min.js.map,
          resource/javascript/loader.min.js,
          test/resource/data/Com/Wui/Framework/Commons,
          index.html

com-wui-framework-gui v2019.0.1
Description: WUI Framework base GUI library
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: resource/css/[package-name]-[version].min.css,
          resource/graphics/Com/Wui/Framework/Gui,
          resource/javascript/[package-name]-[version].d.ts,
          resource/javascript/[package-name]-[version].min.js,
          resource/javascript/[package-name]-[version].min.js.map,
          test/resource/data/Com/Wui/Framework/Gui,
          test/resource/graphics/Com/Wui/Framework/Gui,
          index.html

Oxygen Font N/A
Description: Web font in ttf format
Author: Vernon Adams
License: OFL-1.1, See OFL.txt
Format: font
Location: resource/libs/FontOxygen

com-wui-framework-usercontrols v2019.0.1
Description: WUI Framework library focused on basic User Controls
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: resource/css/[package-name]-[version].min.css,
          resource/graphics/Com/Wui/Framework/UserControls,
          resource/javascript/[package-name]-[version].d.ts,
          resource/javascript/[package-name]-[version].min.js,
          resource/javascript/[package-name]-[version].min.js.map,
          test/resource/graphics/Com/Wui/Framework/UserControls,
          index.html

com-wui-framework-services v2019.0.1
Description: WUI Framework library focused on services and business logic for applications build on WUI Framework
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: [target/]resource/css/[package-name]-[version].min.css,
          [target/]resource/data/Com/Wui/Framework/Services,
          [target/]resource/javascript/[package-name]-[version].d.ts,
          [target/]resource/javascript/[package-name]-[version].min.js,
          [target/]resource/javascript/[package-name]-[version].min.js.map,
          [target/]test/resource/data/Com/Wui/Framework/Services,
          [target/]index.html,
          [target/]wuirunner.config.jsonp,
          [application-name].config.jsonp

[OPTIONAL]
com-wui-framework-selfextractor v2018.1.1
Description: SelfExtractor for WUI Framework's applications
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: executable binary
Location: [application-name][.exe]

[OPTIONAL]
com-wui-framework-launcher v2018.0.0
Description: Launcher for WUI Framework's applications
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: executable binary
Location: [application-name][.exe]

[OPTIONAL]
com-wui-framework-connector v2019.0.0
Description: Stand alone HTTP server for WUI Framework's applications with WebSockets support and built-in web host
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: executable binary and embedded resources
Location: target/resource/libs/WuiConnector/[application-name] Connector[.exe],
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/package.json,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/resource/css/[package-name]-[version].min.css,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/resource/graphics/icon.ico,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/resource/javascript/[package-name]-[version].d.ts,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/resource/javascript/[package-name]-[version].min.js,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/resource/javascript/[package-name]-[version].min.js.map,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/resource/javascript/loader.min.js,
          target/resource/libs/WuiConnector/[application-name] Connector[.exe]/test/resource/data/Com/Wui/Framework/Connector

[OPTIONAL]
com-wui-framework-chromiumre v2018.1.0
Description: Stand alone runtime environment for WUI Framework's applications
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: tool
Location: wuichromiumre

[OPTIONAL]
com-wui-framework-idejre v1.2.0
Description: WUI Framework runtime environment for Java-based IDEs
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: java package
Location: [package-name]-[version].jar

elastic-apm-node >= 2.0.6
Description: The official Elastic APM agent for Node.js
Author: Thomas Watson Steen
License: BSD-2-Clause. See LICENSE
Format: source code
Location: [application-name][.exe]/node_modules/elastic-apm-node

greenlock >= 2.6.7
Description: Let's Encrypt for node.js on npm
Author: AJ ONeal
License: MPL-2.0. See LICENSE
Format: source code
Location: [application-name][.exe]/node_modules/greenlock

ursa-optional >= 0.9.10
Description: RSA public/private key OpenSSL bindings for node and io.js
Author: Dan Bornstein
License: Apache-2.0. See LICENSE.txt
Format: source code
Location: [application-name][.exe]/node_modules/ursa-optional
          resource/libs/ursa-optional

com-wui-framework-hub v2019.0.0
Description: WUI Framework's synchronization hub
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: executable binary and embedded resources
Location: [application-name][.exe],
          robots.txt,
          [application-name][.exe]/package.json,
          [application-name][.exe]/resource/css/[package-name]-[version].min.css,
          [application-name][.exe]/resource/graphics/icon.ico,
          [application-name][.exe]/resource/javascript/[package-name]-[version].d.ts,
          [application-name][.exe]/resource/javascript/[package-name]-[version].min.js,
          [application-name][.exe]/resource/javascript/[package-name]-[version].min.js.map,
          [application-name][.exe]/resource/javascript/loader.min.js,
          [application-name][.exe]/test/resource/data/Com/Wui/Framework/Hub
