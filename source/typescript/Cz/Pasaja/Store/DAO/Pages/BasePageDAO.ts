/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 PASAJA Authors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Cz.Pasaja.Store.DAO.Pages {
    "use strict";
    import BaseDAO = Com.Wui.Framework.Services.DAO.BaseDAO;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class BasePageDAO extends Com.Wui.Framework.Services.DAO.BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Cz/Pasaja/Store/Localization/BasePageLocalization.jsonp";

        protected static getDaoInterfaceClassName($interfaceName : string) : any {
            switch ($interfaceName) {
            case "IBasePageConfiguration":
                return Pages.BasePageDAO;
            default:
                break;
            }
            return BaseDAO;
        }
    }
}
