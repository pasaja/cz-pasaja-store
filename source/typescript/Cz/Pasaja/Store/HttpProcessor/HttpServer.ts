/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 PASAJA Authors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Cz.Pasaja.Store.HttpProcessor {
    "use strict";
    import IRequestConnector = Com.Wui.Framework.Localhost.Interfaces.IRequestConnector;
    import IAppConfiguration = Com.Wui.Framework.Hub.Interfaces.IAppConfiguration;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class HttpServer extends Com.Wui.Framework.Hub.HttpProcessor.HttpServer {

        protected isRequestAllowed($request : any, $hostName : string, $protocol : string, $port : number) : boolean {
            if (StringUtils.Contains($request.url, "/.git/", "/.svn", "/webdav/") ||
                StringUtils.EndsWith($request.url, ".jsp") ||
                StringUtils.EndsWith($request.url, ".cgi")) {
                return false;
            }
            return super.isRequestAllowed($request, $hostName, $protocol, $port);
        }

        protected resolveRequest($url : string, $data : any, $connector : IRequestConnector, $request : any, $keepAlive : boolean) : void {
            if (!ObjectValidator.IsEmptyOrNull($request.headers) &&
                !ObjectValidator.IsEmptyOrNull(this.getAppConfiguration().tunnels.server) &&
                ($request.headers.hasOwnProperty("host") || $request.headers.hasOwnProperty("x-forwarded-host"))) {
                let host : string =
                    $request.headers.hasOwnProperty("host") ? $request.headers.host : $request.headers["x-forwarded-host"];
                if (StringUtils.ContainsIgnoreCase(host, ":")) {
                    host = StringUtils.Substring(host, 0, StringUtils.IndexOf(host, ":"));
                }
                if (this.getAppConfiguration().tunnels.server.hasOwnProperty(host)) {
                    if ($url !== "/") {
                        $url = "/forward/continue";
                    } else {
                        $url = "/forward";
                    }
                }
            }
            super.resolveRequest($url, $data, $connector, $request, $keepAlive);
        }

        protected getAppConfiguration() : IAppConfiguration {
            return <IAppConfiguration>super.getAppConfiguration();
        }
    }
}
