/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 PASAJA Authors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Cz.Pasaja.Store.HttpProcessor {
    "use strict";

    export class HttpResolver extends Com.Wui.Framework.Hub.HttpProcessor.HttpResolver {

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            let indexClass : any = Cz.Pasaja.Store.Controllers.Pages.MainPage;
            if (!Loader.getInstance().getEnvironmentArgs().IsProductionMode()) {
                indexClass = Cz.Pasaja.Store.Index;
            }
            resolvers["/"] = indexClass;
            resolvers["/index"] = indexClass;
            resolvers["/index.html"] = indexClass;
            resolvers["/web/"] = indexClass;
            resolvers["/report/{appId}"] = Com.Wui.Framework.Hub.Controllers.Pages.ReportPageController;

            if (Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed()) {
                return resolvers;
            }

            resolvers["/liveContent/"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.LiveContentResolver;
            resolvers["/ping.txt"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.PingResolver;
            resolvers["/ping.png"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.PingResolver;
            resolvers["/robots.txt"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.FileRequestResolver;
            resolvers["/DynamicImage/{type}/{source}/{settings}/{version}"] =
                Com.Wui.Framework.Hub.HttpProcessor.Resolvers.ImageRequestResolver;
            resolvers["/forward"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.ForwardingResolver;
            resolvers["/forward/continue"] = Com.Wui.Framework.Hub.HttpProcessor.Resolvers.ForwardingResolver;

            return resolvers;
        }
    }
}
