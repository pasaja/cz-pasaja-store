/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 PASAJA Authors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Cz.Pasaja.Store {
    "use strict";

    import HttpServer = Cz.Pasaja.Store.HttpProcessor.HttpServer;
    import HttpResolver = Cz.Pasaja.Store.HttpProcessor.HttpResolver;

    export class Loader extends Com.Wui.Framework.Hub.Loader {

        public static getInstance() : Loader {
            return <Loader>super.getInstance();
        }

        public getHttpResolver() : HttpResolver {
            return <HttpResolver>super.getHttpResolver();
        }

        protected initHttpServer() : HttpServer {
            return new HttpServer();
        }

        protected initResolver() : HttpResolver {
            return new HttpResolver("/");
        }

    }
}
