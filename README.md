# cz-pasaja-store v2019.0.0

> PASAJA data storage server based on WUI Frameworks's hub

## Requirements

This library does not have any special requirements but it depends on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`wui docs` command from the {projectRoot} folder.

## History

### v2019.0.0
Initial release.

## License

This software is owned or controlled by PASAJA Authors.
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
 
See the `LICENSE.txt` file for more details.

---

Author Michal Kelnar, 
Copyright (c) 2019 [PASAJA Authors](http://pasaja.cz/)
